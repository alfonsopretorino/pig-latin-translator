import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
		
	}
	
	@Test
	public void testTranslationEmptyInputPhrase() {
		
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
		
	}
	
	
	@Test
	public void testTranslationStartingWithVowelAndEndingWithY() {
		
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
		
	}
	
	@Test
	public void testTranslationStartingWithUAndEndingWithY() {
		
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
		
	}
	
	@Test
	public void testTranslationStartingWithVowelAndEndingWithVowel() {
		
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
		
	}
	
	@Test
	public void testTranslationStartingWithVowelAndEndingWithConsonant() {
		
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
		
	}
	
	@Test
	public void testTranslationStartingWithConsonant() {
		
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
		
	}
	
	@Test
	public void testTranslationStartingWithMoreConsonant() {
		
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
		
	}
	
	@Test
	public void testTranslationPhraseContainingMoreWordsSeparetedByWhiteSpace() {
		
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
		
	}
	
	@Test
	public void testTranslationPhraseContainingMoreWordsSeparetedByWhiteDash() {
		
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
		
	}

	@Test
	public void testTranslationPhraseContainingMoreWordsEndingWithPunctuation() {
		
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());
		
	}
	
	@Test
	public void testTranslationPhraseupperCase() {
		
		String inputPhrase = "APPLE";
		Translator translator = new Translator(inputPhrase);
		assertEquals("APPLEYAY", translator.translate());
		
	}
	
	@Test
	public void testTranslationPhraseWhereFirstLetterIsupperCase() {
		
		String inputPhrase = "Hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("Ellohay", translator.translate());
		
	}

}
