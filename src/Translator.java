
public class Translator {

	private String phrase = "";
	private String[] arrOfPhrase = {""};
	private boolean dash = false;
	String punctuation = "0";
	private int upper = 0;
	private String lower = "";
	
	public static final String NIL = "nil";
	
	public Translator(String inputPhrase) {
		
		phrase = inputPhrase;
		
	}

	public String getPhrase() {
		
		return phrase;
	}

	public String translate() {
		
		checkAndConvertupperCaseWordsTolower();
		checkAndSplitMultipleWords();
		for(int i = 0; i<=arrOfPhrase.length-1; i++)
		{
			if(endingWithPunctuation(arrOfPhrase[i]))
			{
				arrOfPhrase[i] = savePunctuationandDeleteIt(arrOfPhrase[i]);
			}
			if(startWithVowel(arrOfPhrase[i]))
			{
				if(arrOfPhrase[i].endsWith("y"))
				{
					arrOfPhrase[i] = arrOfPhrase[i] + "nay";
				}else if(endingWithVowel(arrOfPhrase[i]))
				{
					arrOfPhrase[i] = arrOfPhrase[i] + "yay";
				}else if(!endingWithVowel(arrOfPhrase[i]))
				{
					arrOfPhrase[i] = arrOfPhrase[i] + "ay";
				}
			}
			if(!(startWithVowel(arrOfPhrase[i])) && !(nilPhrase(arrOfPhrase[i])))
			{
				arrOfPhrase[i] = moveFirstToEnd(arrOfPhrase[i]) + "ay";
			}
			if(nilPhrase(arrOfPhrase[i]))
			{
				arrOfPhrase[i] = NIL;
			}
			
			arrOfPhrase[i] = checkAndRestorePunctuation(arrOfPhrase[i]);
			arrOfPhrase[i] = restoreUpCaseLetters(arrOfPhrase[i]);
		}
		return checkAndJoinMultipleWords();
	}
	
	
	private boolean nilPhrase(String currentWord)
	{
		return currentWord.equals("");
	}
	
	private boolean startWithVowel(String currentWord)
	{
		return currentWord.startsWith("a") || currentWord.startsWith("e") || currentWord.startsWith("i") || currentWord.startsWith("o") ||currentWord.startsWith("u");
		
	}
	private boolean endingWithVowel(String currentWord)
	{
		return currentWord.endsWith("a") || currentWord.endsWith("e") || currentWord.endsWith("i") || currentWord.endsWith("o") || currentWord.endsWith("u");
	}
	
	private String moveFirstToEnd(String currentWord)
	{
		String first;
		for(int i = 0; i<currentWord.length()-1; i++)
		{
			if(!(startWithVowel(currentWord)))
			{
				first = Character.toString(currentWord.charAt(0));
				currentWord = currentWord + first;
				currentWord = currentWord.replaceFirst(first, "");
			}
			else break;
		}
		return currentWord;
	}
	
	private boolean endingWithPunctuation(String currentWord)
	{
		return currentWord.endsWith(".") || currentWord.endsWith(",") || currentWord.endsWith(";") || currentWord.endsWith(":") || currentWord.endsWith("?") || currentWord.endsWith("!") || currentWord.endsWith("'") || currentWord.endsWith("(") || currentWord.endsWith(")");
	}
	
	private String savePunctuationandDeleteIt(String currentWord) 
	{
		
		char tempPun = currentWord.charAt(currentWord.length() -1);
		punctuation = String.valueOf(tempPun);
		currentWord = currentWord.replaceFirst(punctuation, "");
		
		return currentWord;
	}
	
	private String checkAndRestorePunctuation(String currentWord)
	{
		if(!"0".equals(punctuation))
		{
		    currentWord = currentWord + punctuation;
			punctuation = "0";
		}
		return currentWord;
	}
	
	private void checkAndConvertupperCaseWordsTolower()
	{
		for(int i = 0; i<phrase.length(); i++)
		{
			char tempC = phrase.charAt(i);
			if(Character.isUpperCase(tempC)) {
				upper++;
			}
		}
		lower = phrase.toLowerCase();
	}
	
	private String restoreUpCaseLetters(String currentWord)
	{
		if(upper > lower.length() / 2 && upper != 0 || upper == 1)
		{
			for(int j = 0; j<currentWord.length(); j++)
			{
				char lettera = currentWord.charAt(j);
				String sostituta = String.valueOf(Character.toUpperCase(lettera));
				String dead = String.valueOf(lettera);
				currentWord = currentWord.replaceFirst(dead, sostituta);
				if(upper == 1) break;
			}
		}
		return currentWord;
	}
	
	private void checkAndSplitMultipleWords()
	{
		if(lower.contains("-"))
		{
			dash = true;
			arrOfPhrase = lower.split("-", 0);
		}
		else 
		{
			arrOfPhrase = lower.split(" ", 0);
		}
	}
	
	private String checkAndJoinMultipleWords()
	{
		if(!dash)
		{
			return String.join(" ", arrOfPhrase);
		}
		else
		{
			return String.join("-", arrOfPhrase);
		}
	}
}
